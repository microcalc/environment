#!/bin/bash

docker-compose \
  -f docker-compose.gitlab.yml \
  -f docker-compose.jenkins.yml \
  -f docker-compose.sonar.yml \
  -f docker-compose.artifactory.yml \
  up

#-f docker-compose.nexus.yml \

