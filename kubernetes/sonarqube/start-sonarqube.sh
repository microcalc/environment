#!/bin/bash

#set -e -x

#sleep 10

kubectl create \
  -f sonarqube-bundled-plugins-persistentvolumeclaim.yaml \
  -f sonarqube-conf-persistentvolumeclaim.yaml \
  -f sonarqube-data-persistentvolumeclaim.yaml \
  -f sonarqube-extensions-persistentvolumeclaim.yaml

sleep 10

kubectl create -f sonarqube-service.yaml

sleep 10

kubectl create -f sonarqube-deployment.yaml
