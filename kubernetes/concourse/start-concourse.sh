#!/bin/bash

#set -e -x

rm -rf keys
mkdir -p keys/web keys/worker

ssh-keygen -t rsa -f ./keys/web/tsa_host_key -N ''
ssh-keygen -t rsa -f ./keys/web/session_signing_key -N ''

ssh-keygen -t rsa -f ./keys/worker/worker_key -N ''

cp ./keys/worker/worker_key.pub ./keys/web/authorized_worker_keys
cp ./keys/web/tsa_host_key.pub ./keys/worker


kubectl create secret generic concourse-web-keys \
  --from-file=./keys/web/authorized_worker_keys \
  --from-file=./keys/web/session_signing_key \
  --from-file=./keys/web/session_signing_key.pub \
  --from-file=./keys/web/tsa_host_key \
  --from-file=./keys/web/tsa_host_key.pub

# Se quiser baixar o yml
#kubectl get secret concourse-web-keys -o yaml


kubectl create secret generic concourse-worker-keys \
  --from-file=./keys/worker/tsa_host_key.pub \
  --from-file=./keys/worker/worker_key \
  --from-file=./keys/worker/worker_key.pub

rm -rf keys


#sleep 10

kubectl create \
  -f concourse-db-service.yaml \
  -f concourse-worker-service.yaml \
  -f concourse-web-service.yaml

sleep 10

kubectl create \
  -f concourse-db-deployment.yaml \
  -f concourse-worker-deployment.yaml \
  -f concourse-web-deployment.yaml \



