#!/bin/bash

curl -i -X POST \
  --url http://35.193.189.86:8001/apis/ \
  --data 'name=example-api' \
  --data 'hosts=example.com' \
  --data 'upstream_url=http://artifactory'
