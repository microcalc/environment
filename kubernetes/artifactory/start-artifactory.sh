#!/bin/bash

#set -e -x
kubectl create -f artifactory5-data-persistentvolumeclaim.yaml

sleep 10

kubectl create -f artifactory-service.yaml

sleep 10

kubectl create -f artifactory-deployment.yaml
